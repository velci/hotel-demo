#include <iostream>

#include "include/Hotel.h"
#include "include/Rate.h"
#include "include/Item.h"
#include "include/RateItem.h"
#include "include/TaxPolicy.h"
#include "include/FeePolicy.h"
#include "include/MealPolicy.h"
#include "include/Product.h"
#include "include/AvailabilityProcessor.h"

using namespace std;

int main() {

    // set up data

    Hotel hotel;
    hotel.setName("Eagle Resort");
    hotel.setDescription("Eagle Resort");

    TaxPolicy floridaTax;
    floridaTax.setName("6% Florida Tax");
    floridaTax.setDescription("6% Florida Tax");
    floridaTax.setCalculationMethod(TaxPolicy::PERCENT);
    floridaTax.setCalculationAmount(.06);

    TaxPolicy collierTouristTax;
    collierTouristTax.setName("5% Collier County Tourist Tax");
    collierTouristTax.setDescription("5% Collier County Tourist Tax");
    collierTouristTax.setCalculationMethod(TaxPolicy::PERCENT);
    collierTouristTax.setCalculationAmount(.05);

    FeePolicy resortFee;
    resortFee.setName("$5 Resort Fee");
    resortFee.setDescription("$5 Resort Fee");
    resortFee.setCalculationMethod(FeePolicy::FLAT);
    resortFee.setCalculationAmount(5);

    FeePolicy parkingFee;
    parkingFee.setName("$2.50 Parking Fee");
    parkingFee.setDescription("$2.50 Parking Fee");
    parkingFee.setCalculationMethod(FeePolicy::FLAT);
    parkingFee.setCalculationAmount(2.5);

    MealPolicy continentalBreakfastMeal;
    continentalBreakfastMeal.setName("Continental Breakfast");
    continentalBreakfastMeal.setDescription("Continental Breakfast");
    continentalBreakfastMeal.setCalculationMethod(MealPolicy::FLAT);
    continentalBreakfastMeal.setCalculationAmount(5);

    Rate standardRate;
    standardRate.setName("Standard Rate");
    standardRate.setDescription("Standard Rate");
    standardRate.setAmount(100);
    standardRate.setMealPolicy(continentalBreakfastMeal);
    standardRate.setFeesIncluded(false);
    standardRate.setTaxesIncluded(false);
    standardRate.setMealIncluded(false);
    hotel.addRate(standardRate);

    Rate floridaRate;
    floridaRate.setName("Florida Resident Rate");
    floridaRate.setDescription("A special rate for residents of Florida.  Requires valid Florida ID at check-in");
    floridaRate.setAmount(106);
    floridaRate.setMealPolicy(continentalBreakfastMeal);
    floridaRate.setFeesIncluded(false);
    floridaRate.setTaxesIncluded(true);
    floridaRate.setMealIncluded(false);
    hotel.addRate(standardRate);

    Item kingRoom;
    kingRoom.setName("King Room");
    kingRoom.setDescription("It's time to treat yourself like a king!  This luxurious room features a King Bed.");
    kingRoom.addTaxPolicy(floridaTax);
    kingRoom.addTaxPolicy(collierTouristTax);
    kingRoom.addFeePolicy(resortFee);
    kingRoom.addFeePolicy(parkingFee);
    hotel.addItem(kingRoom);

    Item doubleRoom;
    doubleRoom.setName("Double Room");
    doubleRoom.setDescription("Bring the whole family!  This room features two Double Beds for the cost-conscious.");
    doubleRoom.addTaxPolicy(floridaTax);
    doubleRoom.addTaxPolicy(collierTouristTax);
    doubleRoom.addFeePolicy(resortFee);
    doubleRoom.addFeePolicy(parkingFee);
    hotel.addItem(doubleRoom);

    RateItem standardKing(standardRate, kingRoom);
    hotel.addRateItem(standardKing);

    RateItem standardDouble(standardRate, doubleRoom);
    hotel.addRateItem(standardDouble);

    RateItem floridaKing(floridaRate, kingRoom);
    hotel.addRateItem(floridaKing);

    RateItem floridaDouble(floridaRate, doubleRoom);
    hotel.addRateItem(floridaDouble);

    vector<Product> products = AvailabilityProcessor::getAvailability(hotel);

    cout << "Welcome to " << hotel.getName() << endl;
    cout << endl;

    for(vector<Product>::iterator it = products.begin(); it != products.end(); ++it) {
        cout << (*it).getItem().getName() << " - " << (*it).getRate().getName() << endl;
        cout << (*it).getItem().getDescription() << endl;
        cout << (*it).getRate().getDescription() << endl;

        cout << "Base Price: " << (*it).getBasePrice() << endl;
        cout << "Taxes:      " << (*it).getTaxes() << endl;
        cout << "Fees:       " << (*it).getFees() << endl;
        cout << "Total:      " << (*it).getTotalPrice() << endl;
        cout << endl;
    }
}
