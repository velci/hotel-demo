#include "../include/FeePolicy.h"

#include <string>

using namespace std;

FeePolicy::FeePolicy() {
    //ctor
}

FeePolicy::~FeePolicy() {
    //dtor
}

string FeePolicy::getName() {
    return name;
}

void FeePolicy::setName(string name) {
    this->name = name;
}

string FeePolicy::getDescription() {
    return description;
}

void FeePolicy::setDescription(string description) {
    this->description = description;
}

int FeePolicy::getCalculationMethod() {
    return calculationMethod;
}

void FeePolicy::setCalculationMethod(int calculationMethod) {
    this->calculationMethod = calculationMethod;
}

float FeePolicy::getCalculationAmount() {
    return calculationAmount;
}

void FeePolicy::setCalculationAmount(float calculationAmount) {
    this->calculationAmount = calculationAmount;
}
