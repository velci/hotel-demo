#include "../include/AvailabilityProcessor.h"
#include "../include/Product.h"
#include "../include/TaxPolicy.h"
#include "../include/FeePolicy.h"
#include "../include/MealPolicy.h"
#include <vector>

using namespace std;

AvailabilityProcessor::AvailabilityProcessor() {
    //ctor
}

AvailabilityProcessor::~AvailabilityProcessor() {
    //dtor
}

void AvailabilityProcessor::calculateBasePrice(Product& product) {
    float basePrice = 0;

    // simple for now...
    basePrice += product.getRate().getAmount();

    product.setBasePrice(basePrice);
}

void AvailabilityProcessor::calculateTaxes(Product& product) {
    float taxes = 0;

    if (!product.getRate().areTaxesIncluded()) {
        vector<TaxPolicy> taxPolicies = product.getItem().getTaxPolicies();

        for(vector<TaxPolicy>::iterator it = taxPolicies.begin(); it != taxPolicies.end(); ++it) {
            switch ((*it).getCalculationMethod()) {
            case TaxPolicy::FLAT:
                taxes += (*it).getCalculationAmount();
                break;
            case TaxPolicy::PERCENT:
                taxes += product.getBasePrice() * (*it).getCalculationAmount();
                break;
            }
        }
    }
    product.setTaxes(taxes);

}

void AvailabilityProcessor::calculateFees(Product& product) {
    float fees = 0;

    if (!product.getRate().areFeesIncluded()) {
        vector<FeePolicy> feePolicies = product.getItem().getFeePolicies();

        for(vector<FeePolicy>::iterator it = feePolicies.begin(); it != feePolicies.end(); ++it) {
            switch ((*it).getCalculationMethod()) {
            case FeePolicy::FLAT:
                fees += (*it).getCalculationAmount();
                break;
            case FeePolicy::PERCENT:
                fees += product.getBasePrice() * (*it).getCalculationAmount();
                break;
            }
        }
    }

    if (!product.getRate().isMealIncluded()) {
        // meal policy amount is summed into the fees bucket
        MealPolicy mealPolicy = product.getRate().getMealPolicy();

        switch (mealPolicy.getCalculationMethod()) {
        case FeePolicy::FLAT:
            fees += mealPolicy.getCalculationAmount();
            break;
        case FeePolicy::PERCENT:
            fees += product.getBasePrice() * mealPolicy.getCalculationAmount();
            break;
        }
    }
    product.setFees(fees);
}

vector<Product> AvailabilityProcessor::getAvailability(Hotel hotel) {
    vector<Product> products;

    vector<RateItem> rateItems = hotel.getRateItems();

    for(vector<RateItem>::iterator it = rateItems.begin(); it != rateItems.end(); ++it) {
        Product product((*it).getRate(), (*it).getItem());

        calculateBasePrice(product);
        calculateTaxes(product);
        calculateFees(product);

        products.push_back(product);
    }

    return products;
}
