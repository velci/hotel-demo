#include "../include/Rate.h"
#include "../include/MealPolicy.h"

#include <string>

using namespace std;

Rate::Rate() {
    //ctor
}

Rate::~Rate() {
    //dtor
}

string Rate::getName() {
    return name;
}

void Rate::setName(string name) {
    this->name = name;
}

string Rate::getDescription() {
    return description;
}

void Rate::setDescription(string description) {
    this->description = description;
}

float Rate::getAmount() {
    return amount;
}

void Rate::setAmount(float amount) {
    this->amount = amount;
}

MealPolicy Rate::getMealPolicy() {
    return mealPolicy;
}

void Rate::setMealPolicy(MealPolicy mealPolicy) {
    this->mealPolicy = mealPolicy;
}

bool Rate::areTaxesIncluded() {
    return taxesIncluded;
}

void Rate::setTaxesIncluded(bool taxesIncluded) {
    this->taxesIncluded = taxesIncluded;
}

bool Rate::areFeesIncluded() {
    return feesIncluded;
}

void Rate::setFeesIncluded(bool feesIncluded) {
    this->feesIncluded = feesIncluded;
}

bool Rate::isMealIncluded() {
    return mealIncluded;
}

void Rate::setMealIncluded(bool mealIncluded) {
    this->mealIncluded = mealIncluded;
}
