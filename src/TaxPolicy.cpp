#include <string>
#include "../include/TaxPolicy.h"

using namespace std;

TaxPolicy::TaxPolicy() {
    //ctor
}

TaxPolicy::~TaxPolicy() {
    //dtor
}

string TaxPolicy::getName() {
    return name;
}

void TaxPolicy::setName(string name) {
    this->name = name;
}

string TaxPolicy::getDescription() {
    return description;
}

void TaxPolicy::setDescription(string description) {
    this->description = description;
}

int TaxPolicy::getCalculationMethod() {
    return calculationMethod;
}

void TaxPolicy::setCalculationMethod(int calculationMethod) {
    this->calculationMethod = calculationMethod;
}

float TaxPolicy::getCalculationAmount() {
    return calculationAmount;
}

void TaxPolicy::setCalculationAmount(float calculationAmount) {
    this->calculationAmount = calculationAmount;
}
