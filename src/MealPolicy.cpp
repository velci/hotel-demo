#include "../include/MealPolicy.h"

#include <string>

using namespace std;

MealPolicy::MealPolicy() {
    //ctor
}

MealPolicy::~MealPolicy() {
    //dtor
}

string MealPolicy::getName() {
    return name;
}

void MealPolicy::setName(string name) {
    this->name = name;
}

string MealPolicy::getDescription() {
    return description;
}

void MealPolicy::setDescription(string description) {
    this->description = description;
}

int MealPolicy::getCalculationMethod() {
    return calculationMethod;
}

void MealPolicy::setCalculationMethod(int calculationMethod) {
    this->calculationMethod = calculationMethod;
}

float MealPolicy::getCalculationAmount() {
    return calculationAmount;
}

void MealPolicy::setCalculationAmount(float calculationAmount) {
    this->calculationAmount = calculationAmount;
}
