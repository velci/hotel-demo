#include "../include/Hotel.h"
#include "../include/Rate.h"
#include "../include/Item.h"
#include "../include/RateItem.h"

#include <vector>

using namespace std;

Hotel::Hotel() {

}

Hotel::~Hotel() {
    //dtor
}

string Hotel::getName() {
    return name;
}

void Hotel::setName(std::string name) {
    this->name = name;
}

string Hotel::getDescription() {
    return description;
}

void Hotel::setDescription(std::string description) {
    this->description = description;
}

vector<Rate> Hotel::getRates() {
    return rates;
}

void Hotel::addRate(Rate rate) {
    rates.push_back(rate);
}

vector<Item> Hotel::getItems() {
    return items;
}

void Hotel::addItem(Item item) {
    items.push_back(item);
}

vector<RateItem> Hotel::getRateItems() {
    return rateItems;
}

void Hotel::addRateItem(RateItem rateItem) {
    rateItems.push_back(rateItem);
}
