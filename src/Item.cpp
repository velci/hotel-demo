#include "../include/Item.h"
#include "../include/FeePolicy.h"
#include "../include/TaxPolicy.h"

#include <string>
#include <vector>

using namespace std;

Item::Item() {
    //ctor
}

Item::~Item() {
    //dtor
}

std::string Item::getName() {
    return name;
}

void Item::setName(std::string name) {
    this->name = name;
}

std::string Item::getDescription() {
    return description;
}

void Item::setDescription(std::string description) {
    this->description = description;
}

vector<FeePolicy> Item::getFeePolicies() {
    return feePolicies;
}

void Item::addFeePolicy(FeePolicy feePolicy) {
    feePolicies.push_back(feePolicy);
}

vector<TaxPolicy> Item::getTaxPolicies() {
    return taxPolicies;
}

void Item::addTaxPolicy(TaxPolicy taxPolicy) {
    taxPolicies.push_back(taxPolicy);
}
