#include "../include/Product.h"

#include <string>

using namespace std;

Product::Product(Rate rate, Item item) : RateItem(rate, item) {

}

Product::~Product() {
    //dtor
}

float Product::getBasePrice() {
    return basePrice;
}

void Product::setBasePrice(float basePrice) {
    this->basePrice = basePrice;
}

float Product::getTaxes() {
    return taxes;
}

void Product::setTaxes(float taxes) {
    this->taxes = taxes;
}

float Product::getFees() {
    return fees;
}

void Product::setFees(float fees) {
    this->fees = fees;
}

float Product::getTotalPrice() {
    return basePrice + taxes + fees;
}
