#include "../include/RateItem.h"
#include "../include/Rate.h"
#include "../include/Item.h"

RateItem::RateItem(Rate rate, Item item) : rate(rate), item(item) {
    //ctor
}

RateItem::~RateItem() {
    //dtor
}

Rate RateItem::getRate() {
    return rate;
}

Item RateItem::getItem() {
    return item;
}
