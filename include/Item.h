#ifndef ITEM_H
#define ITEM_H

#include "FeePolicy.h"
#include "TaxPolicy.h"

#include <string>
#include <vector>

/**
    Information about the physical product
*/
class Item {
    public:
        Item();
        virtual ~Item();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        std::vector<FeePolicy> getFeePolicies();
        void addFeePolicy(FeePolicy feePolicy);
        std::vector<TaxPolicy> getTaxPolicies();
        void addTaxPolicy(TaxPolicy taxPolicy);
    protected:
    private:
        std::string name;
        std::string description;
        std::vector<FeePolicy> feePolicies;
        std::vector<TaxPolicy> taxPolicies;
};

#endif // ITEM_H
