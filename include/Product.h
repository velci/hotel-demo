#ifndef PRODUCT_H
#define PRODUCT_H

#include "RateItem.h"
#include "Rate.h"
#include "Item.h"

class Product : public RateItem {
    public:
        Product(Rate rate, Item item);
        virtual ~Product();
        float getBasePrice();
        void setBasePrice(float basePrice);
        float getTaxes();
        void setTaxes(float taxes);
        float getFees();
        void setFees(float fees);
        float getTotalPrice();
    protected:
    private:
        float basePrice;
        float taxes;
        float fees;
};

#endif // PRODUCT_H
