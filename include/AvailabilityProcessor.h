#ifndef AVAILABILITYPROCESSOR_H
#define AVAILABILITYPROCESSOR_H

#include <vector>

#include "Hotel.h"
#include "Product.h"

/**
    Retrieves availability information for a hotel.
*/
class AvailabilityProcessor {
    public:
        AvailabilityProcessor();
        virtual ~AvailabilityProcessor();
        static std::vector<Product> getAvailability(Hotel hotel);
    protected:
    private:
        static void calculateBasePrice(Product& product);
        static void calculateTaxes(Product& product);
        static void calculateFees(Product& product);
};

#endif // AVAILABILITYPROCESSOR_H
