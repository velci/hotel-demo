#ifndef TAXPOLICY_H
#define TAXPOLICY_H

#include <string>

/**
    Defines a tax (e.g. state or local tax)
*/
class TaxPolicy {
    public:
        enum CalculationMethod {
            FLAT,
            PERCENT
        };
        TaxPolicy();
        virtual ~TaxPolicy();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        int getCalculationMethod();
        void setCalculationMethod(int calculationMethod);
        float getCalculationAmount();
        void setCalculationAmount(float calculationAmount);
    protected:
    private:
        std::string name;
        std::string description;
        int calculationMethod;
        float calculationAmount;
};

#endif // TAXPOLICY_H
