#ifndef FEEPOLICY_H
#define FEEPOLICY_H

#include <string>

/**
    Defines a fee (e.g. parking fee, resort fee)
*/
class FeePolicy {
    public:
        enum CalculationMethod {
            FLAT,
            PERCENT
        };
        FeePolicy();
        virtual ~FeePolicy();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        int getCalculationMethod();
        void setCalculationMethod(int calculationMethod);
        float getCalculationAmount();
        void setCalculationAmount(float calculationAmount);
    protected:
    private:
        std::string name;
        std::string description;
        int calculationMethod;
        float calculationAmount;
};

#endif // FEEPOLICY_H
