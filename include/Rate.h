#ifndef RATE_H
#define RATE_H

#include <string>
#include "MealPolicy.h"

/**
    Defines the pricing of a product
*/
class Rate {
    public:
        Rate();
        virtual ~Rate();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        float getAmount();
        void setAmount(float amount);
        MealPolicy getMealPolicy();
        void setMealPolicy(MealPolicy mealPolicy);
        bool areTaxesIncluded();
        void setTaxesIncluded(bool taxesIncluded);
        bool areFeesIncluded();
        void setFeesIncluded(bool feesIncluded);
        bool isMealIncluded();
        void setMealIncluded(bool mealIncluded);
    protected:
    private:
        std::string name;
        std::string description;
        float amount;
        MealPolicy mealPolicy;
        bool taxesIncluded;
        bool feesIncluded;
        bool mealIncluded;
};

#endif // RATE_H
