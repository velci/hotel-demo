#ifndef HOTEL_H
#define HOTEL_H

#include <vector>
#include <string>

#include "Rate.h"
#include "Item.h"
#include "RateItem.h"

class Hotel {
    public:
        Hotel();
        virtual ~Hotel();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        std::vector<Rate> getRates();
        void addRate(Rate rate);
        std::vector<Item> getItems();
        void addItem(Item item);
        std::vector<RateItem> getRateItems();
        void addRateItem(RateItem rateItem);
    protected:
    private:
        std::string name;
        std::string description;
        std::vector<Rate> rates;
        std::vector<Item> items;
        std::vector<RateItem> rateItems;
};

#endif // HOTEL_H
