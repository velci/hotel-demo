#ifndef MEALPOLICY_H
#define MEALPOLICY_H

#include <string>

/**
    Defines a meal plan (e.g. breakfast buffet)
*/
class MealPolicy {
    public:
        enum CalculationMethod {
            FLAT,
            PERCENT
        };
        MealPolicy();
        virtual ~MealPolicy();
        std::string getName();
        void setName(std::string name);
        std::string getDescription();
        void setDescription(std::string description);
        int getCalculationMethod();
        void setCalculationMethod(int calculationMethod);
        float getCalculationAmount();
        void setCalculationAmount(float calculationAmount);
    protected:
    private:
        std::string name;
        std::string description;
        int calculationMethod;
        float calculationAmount;
};

#endif // MEALPOLICY_H
