#ifndef RATEITEM_H
#define RATEITEM_H

#include "Rate.h"
#include "Item.h"

/**
    A pairing of a rate with an item.  Holds no other significant data.
*/
class RateItem {
    public:
        RateItem(Rate rate, Item item);
        virtual ~RateItem();
        Rate getRate();
        Item getItem();
    protected:
    private:
        Rate rate;
        Item item;
};

#endif // RATEITEM_H
